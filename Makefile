##
## Makefile for tree in /home/kirsz_m
## 
## Made by Maxime Kirsz
## Login   <kirsz_m@epitech.net>
## 
## Started on  Mon Jul 18 17:56:16 2016 Maxime Kirsz
## Last update Mon Jul 18 17:56:18 2016 Maxime Kirsz
##

CC	= gcc

RM	= rm -rf

SRCS	= handle_list.c \
	nbr_tools.c \
	list_tools.c \
	str_utils.c \
	tree_tools.c \
	my_str_to_wordtab.c \
	str_tools.c \
	tree.c

OBJS	= $(SRCS:.c=.o)

NAME	= tree

CFLAGS	+= -Iinclude/ -Wall -Wextra -Werror

LDFLAGS	=

all: $(NAME)

$(NAME): $(OBJS)
	$(CC) -o $(NAME) $(OBJS) $(LDFLAGS)

clean:
	$(RM) $(OBJS)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
