/*
** list_tools.c for tree in /home/kirsz_m
**
** Made by Maxime Kirsz
** Login   <kirsz_m@epitech.net>
**
** Started on  Mon Jul 18 17:56:39 2016 Maxime Kirsz
** Last update Mon Jul 18 17:56:40 2016 Maxime Kirsz
*/

#include <stdlib.h>

int		how_many(const char *str, char c)
{
  int		count;
  int		i;

  i = 0;
  count = 0;
  while (str[i])
    {
      if (str[i] == c)
	count++;
      i++;
    }
  return (count);
}
