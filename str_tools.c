/*
** str_tools.c for tree in /home/kirsz_m
**
** Made by Maxime Kirsz
** Login   <kirsz_m@epitech.net>
**
** Started on  Mon Jul 18 17:57:15 2016 Maxime Kirsz
** Last update Mon Jul 18 17:59:51 2016 Maxime Kirsz
*/

#include <stdlib.h>
#include "tree.h"

char	*strcat_custom(const char *dest, const char *src)
{
  char	*new;
  int	i;
  int	j;

  i = 0;
  j = 0;
  if ((new = malloc(sizeof(*new) * (my_strlen(dest) + my_strlen(src) + 2))) == NULL)
    return (MALLOC_FAILED);
  while (dest[i])
    {
      new[i] = dest[i];
      i++;
    }
  new[i] = '/';
  i++;
  while (src[j])
    {
      new[i] = src[j];
      i++;
      j++;
    }
  new[i] = 0;
  return (new);
}

int	strcmp_path(char *s1, char *s2)
{
  int	i;

  i = 0;
  if (s1 == NULL || s2 == NULL)
    return (-1);
  while (s1[i] == s2[i] && i <= 4)
    i++;
  if (i == 5)
    return (0);
  else
    return (s1[i] - s2[i]);
}

char	*lowcase(const char *str)
{
  int	i;
  char	*new;


  i = 0;
  if ((new = malloc(sizeof(char) * my_strlen(str) + 1)) == NULL)
    return (MALLOC_FAILED);
  while (str[i])
    {
      if (str[i] >= 'A' && str[i] <= 'Z')
	new[i] = str[i] + 32;
      else
	new[i] = str[i];
      i++;
    }
  new[i] = 0;
  return (new);
}

int	my_strcmp_custom(const char *s1, const char *s2)
{
  int	i;
  char	*ss1;
  char	*ss2;

  ss1 = lowcase(s1);
  ss2 = lowcase(s2);
  i = 0;
  while (ss1[i] && ss2[i] && ss1[i] == ss2[i])
    i++;
  i = ss1[i] - ss2[i];
  free(ss1);
  free(ss2);
  return (i);
}
