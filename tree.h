/*
** proj.h for projTester in /home/kirsz_m
**
** Made by Maxime Kirsz
** Login   <kirsz_m@epitech.net>
**
** Started on  Wed Jun 22 19:44:35 2016 Maxime Kirsz
** Last update Mon Jul 18 17:58:35 2016 Maxime Kirsz
*/

#ifndef TREE_H_
# define TREE_H_

#undef SUCCESS
# define SUCCESS 0

#undef MALLOC_FAILURE
# define MALLOC_FAILURE (-1)

#undef IS_DIREC
# define IS_DIREC (0)

#undef IS_FILE
# define IS_FILE (1)

#undef WRONG_ARGS
# define WRONG_ARGS (84)

#undef OPENDIR_FAILURE
# define OPENDIR_FAILURE (-1)

#undef FAILURE
# define FAILURE (-1)

#undef MALLOC_FAILED
# define MALLOC_FAILED (NULL)

#undef EMPTY_TAB
# define EMPTY_TAB (NULL)

typedef struct	s_file
{
  char		*name;
  int		id;
  struct s_file	*next;
  struct s_file	*prev;
}		t_file;

typedef struct	s_beacon
{
  t_file	*head;
  t_file	*tail;
  char		*bin;
  char		**path_tab;
  int		size;
}		t_beacon;

/*
** handle_list.c
*/
int	tail_add(t_beacon *, char *, int);
void	free_list(t_file *);
void	aff_dir(const char *, int);
void	aff_reg(const t_file *, int);
void	sort_list(t_beacon *);

/*
** str_utils.c
*/
void	my_putchar(const char);
void	my_putstr(const char *);
int	my_strlen(const char *);
char	*my_strcat(const char*, const char*);
int	my_strcmp(const char *, const char *);

/*
** list_tools.c
*/
int	how_many(const char *, char);

/*
** str_tools.c
*/
char	*strcat_custom(const char *, const char *);
int	my_strcmp_custom(const char *, const char *);

/*
** str_to_wordtab.c
*/
char	**my_str_to_wordtab(const char *, char);

/*
** tree_tools.c
*/
void	init_list(t_beacon *);
int	is_not_dot(const char *);

/*
** tree.c
*/
int	tree_lobby(const char *, int);

#endif /* !TREE_H_ */
