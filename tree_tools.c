/*
** tree_tools.c for tree in /home/kirsz_m
**
** Made by Maxime Kirsz
** Login   <kirsz_m@epitech.net>
**
** Started on  Mon Jul 18 17:58:07 2016 Maxime Kirsz
** Last update Mon Jul 18 17:59:48 2016 Maxime Kirsz
*/

#include <stdlib.h>
#include "tree.h"

void	init_list(t_beacon *beacon)
{
  beacon->head = NULL;
  beacon->tail = NULL;
  beacon->size = 0;
}

int	is_not_dot(const char *file_name)
{
  if (file_name[0] != '.')
    return (1);
  return (0);
}
