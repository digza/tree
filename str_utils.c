/*
** str_utils.c for tree in /home/kirsz_m
**
** Made by Maxime Kirsz
** Login   <kirsz_m@epitech.net>
**
** Started on  Mon Jul 18 17:57:27 2016 Maxime Kirsz
** Last update Mon Jul 18 17:59:51 2016 Maxime Kirsz
*/

#include <unistd.h>
#include <stdlib.h>
#include "tree.h"

void	my_putchar(const char c)
{
  write(1, &c, 1);
}

void	my_putstr(const char *str)
{
  int	index;

  index = 0;
  while (str[index])
    my_putchar(str[index++]);
}

int	my_strlen(const char *str)
{
  int	index;

  index = 0;
  while (str[index++]);
  return (index);
}

char	*my_strcat(const char *dest, const char *src)
{
  int	concat_index;
  int	src_index;
  char	*concat;

  concat_index = 0;
  src_index = 0;
  if ((concat = malloc(sizeof(char) * (my_strlen(dest) + my_strlen(src) + 2))) == NULL)
    return (NULL);
  while (dest[concat_index])
    {
      concat[concat_index] = dest[concat_index];
      concat_index++;
    }
  concat[concat_index++] = '/';
  while (src[src_index])
    concat[concat_index++] = src[src_index++];
  concat[concat_index] = '\0';
  return (concat);
}

int	my_strcmp(const char *cmp,
		  const char *cmped)
{
  int	index;

  index = 0;
  if (cmp == NULL || cmped == NULL)
    return (-1);
  while (cmp[index] == cmped[index] && cmped[index] && cmp[index])
    index++;
  return (cmp[index] - cmped[index]);
}
