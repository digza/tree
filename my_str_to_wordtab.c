/*
** my_str_to_wordtab.c for tree in /home/kirsz_m
**
** Made by Maxime Kirsz
** Login   <kirsz_m@epitech.net>
**
** Started on  Mon Jul 18 17:56:51 2016 Maxime Kirsz
** Last update Mon Jul 18 17:59:51 2016 Maxime Kirsz
*/

#include <stdlib.h>
#include "tree.h"

static int	set_index(const char *str, int i, char token)
{
  while ((str[i] == token || str[i] == '\t' || str[i] == ' ') && str[i] != '\0')
    i++;
  return (i);
}

static int	word_count(const char *str, char token)
{
  int	i;
  int	k;

  i = 0;
  k = 0;
  while (str[i] != '\0')
    {
      if (str[i] != token && str[i] != '\t' && str[i] != ' ')
	{
	  k++;
	  while (str[i] != token && str[i] !='\t' && str[i] != ' ' && str[i])
	    i++;
	}
      else
	i++;
    }
  return (k);
}

static int	char_count(const char *str, int i, char token)
{
  int	k;
  k = 0;
  while (str[i] && str[i] != token && str[i] != 9 && str[i] != ' ')
    {
      i++;
      k++;
    }
  return (i);
}

char	**my_str_to_wordtab(const char *str, char token)
{
  char	**tab;
  int	i;
  int	j;
  int	k;

  j = 0;
  i = 0;
  if (str[0] == 0)
    return (EMPTY_TAB);
  while (str[i] == token || str[i] == '\t' || str[i] == ' ')
    i++;
  if ((tab = malloc(sizeof(char *) * (word_count(str, token) + 1))) == NULL)
    return (MALLOC_FAILED);
  while (str[i] != '\0')
    {
      k = 0;
      if ((tab[j] = malloc(sizeof(char) * (char_count(str, i, token) + 1))) == NULL)
        return (MALLOC_FAILED);
      while (str[i] && str[i] != token && str[i] != ' ' && str[i] != '\t')
	tab[j][k++] = str[i++];
      tab[j][k] = '\0';
      i = set_index(str, i, token);
      j++;
    }
  tab[j] = NULL;
  return (tab);
}
