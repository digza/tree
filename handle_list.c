/*
** handle_list.c for tree in /home/kirsz_m
**
** Made by Maxime Kirsz
** Login   <kirsz_m@epitech.net>
**
** Started on  Mon Jul 18 17:56:27 2016 Maxime Kirsz
** Last update Mon Jul 18 17:58:50 2016 Maxime Kirsz
*/

#include <stdio.h>
#include <stdlib.h>
#include "tree.h"

int		tail_add(t_beacon *beacon, char *name, int elem_id)
{
  t_file	*new_elem;

  if ((new_elem = malloc(sizeof(*new_elem))) == NULL)
    return (MALLOC_FAILURE);
  new_elem->name = name;
  new_elem->id = elem_id;
  if (beacon->head == NULL && beacon->tail == NULL)
    {
      new_elem->prev = NULL;
      beacon->head = new_elem;
      beacon->tail = new_elem;
    }
  else
    {
      new_elem->prev = beacon->tail;
      beacon->tail->next = new_elem;
      beacon->tail = beacon->tail->next;
    }
  new_elem->next = NULL;
  beacon->size++;
  return (SUCCESS);
}

void	free_list(t_file *elem)
{
  if (!elem)
    return ;
  if (elem->next)
    free_list(elem->next);
  if (elem)
    free(elem);
}

void	aff_dir(const char *dir_name, int dash)
{
  int	i;

  i = my_strlen(dir_name) - 1;
  while (dash > 0)
    {
      my_putstr("-----");
      dash--;
    }
  if (how_many(dir_name, '/') == 0)
    printf("%s/\n", dir_name);
  else
    {
      while (dir_name[i] != '/')
	i--;
      i++;
      my_putstr(dir_name + i);
      my_putstr("/\n");
    }
}

void	aff_reg(const t_file *elem, int dash)
{
  while (dash >= 0)
    {
      my_putstr("-----");
      dash--;
    }
  my_putstr(elem->name);
  my_putstr("\n");
}

void		sort_list(t_beacon *beacon)
{
  t_file	*elem;
  char		*tmp;
  int		tmp_id;

  if (beacon->head == NULL)
    return ;
  elem = beacon->head;
  while (elem->next)
    {
      if (my_strcmp_custom(elem->name, elem->next->name) > 0)
	{
	  tmp = elem->next->name;
	  tmp_id = elem->next->id;
	  elem->next->name = elem->name;
	  elem->next->id = elem->id;
	  elem->name = tmp;
	  elem->id = tmp_id;
	  elem = beacon->head;
	}
      else
	elem = elem->next;
    }
}
