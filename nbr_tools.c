/*
** nbr_tools.c for tree in /home/kirsz_m
**
** Made by Maxime Kirsz
** Login   <kirsz_m@epitech.net>
**
** Started on  Mon Jul 18 17:57:02 2016 Maxime Kirsz
** Last update Mon Jul 18 17:59:51 2016 Maxime Kirsz
*/

#include <stdlib.h>
#include "tree.h"

void	my_putnbr(int number)
{
  if (number < 0)
    my_putchar('-');
  if (number > 9)
    my_putnbr(number / 10);
  my_putchar(number % 10 + '0');
}

int	my_getnbr(const char *str)
{
  int	result;
  int	index;

  index = 0;
  result = 0;
  if (str == NULL)
    return (FAILURE);
  while (str[index])
    {
      if (str[index] < '0' || str[index] > '9')
	return (FAILURE);
      result = result * 10 + str[index++] - '0';
    }
  return (result);
}
