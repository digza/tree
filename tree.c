/*
** tree.c for tree in /home/kirsz_m
**
** Made by Maxime Kirsz
** Login   <kirsz_m@epitech.net>
**
** Started on  Mon Jul 18 17:57:58 2016 Maxime Kirsz
** Last update Mon Jul 18 17:59:50 2016 Maxime Kirsz
*/

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <dirent.h>
#include "tree.h"

static int	set_file_type(struct stat buf)
{
  if ((buf.st_mode & S_IFMT) == S_IFDIR)
    return (IS_DIREC);
  return (IS_FILE);
}

static void     display_loop(const t_file *elem, const char *dir_name, int dash)
{
  while (elem)
    {
      if (elem->id == 0)
	tree_lobby(my_strcat(dir_name, elem->name), dash + 1);
      else
	aff_reg(elem, dash);
      elem = elem->next;
    }
}

int	tree_lobby(const char *dir_name, int dash)
{
  DIR		*directory;
  struct dirent	*file;
  struct stat	buf;
  char		*cat;
  t_file	*elem;
  t_beacon	*beacon;

  if ((beacon = malloc(sizeof(*beacon))) == NULL)
    exit(84);
  init_list(beacon);
  if ((directory = opendir(dir_name)) == NULL)
    exit(84);
  while ((file = readdir(directory)) != NULL)
    {
      if ((cat = my_strcat(dir_name, file->d_name)) == NULL)
	exit(84);
      if (stat(cat, &buf) != -1 && is_not_dot(file->d_name))
	if (tail_add(beacon, file->d_name, set_file_type(buf)))
	  exit(84);
      free(cat);
    }
  elem = beacon->head;
  sort_list(beacon);
  aff_dir(dir_name, dash);
  display_loop(elem, dir_name, dash);
  closedir(directory);
  free_list(beacon->head);
  free(beacon);
  return (SUCCESS);
}


int		main(int ac, char **av)
{
  t_beacon	*beacon;

  if ((beacon = malloc(sizeof(*beacon))) == NULL)
    return (MALLOC_FAILURE);
  init_list(beacon);
  if (ac != 2)
    return (WRONG_ARGS);
  else
    tree_lobby(av[1], 0);
  free_list(beacon->head);
  free(beacon);
  return (SUCCESS);
}
